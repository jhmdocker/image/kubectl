ARG UBUNTU_VERSION=ND
FROM registry.gitlab.com/jhmdocker/image/ubuntu:${UBUNTU_VERSION}

ARG KUBECTL_VERSION=ND
ENV KUBECTL_VERSION=$KUBECTL_VERSION

RUN echo KUBECTL_VERSION=$KUBECTL_VERSION && \
    cd /usr/local/bin && \
    wget -q https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x kubectl && \
    echo END
