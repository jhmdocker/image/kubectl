UBUNTU_VERSION=24.04
VERSION=v1.31.1
IMG=registry.gitlab.com/jhmdocker/image/kubectl

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg KUBECTL_VERSION=$(VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
